"use strict";

// Ajoute un événement "click" sur le bouton "Ajouter"
document.getElementById("addSkill").addEventListener("click", function () {
  // Récupère la valeur de l'input de compétence et la formate
  let input = document.getElementById("skillInput");
  let skill = capitalizeFirstLetter(input.value.trim());

  // Vérifie que la compétence n'est pas vide et qu'il y a moins de 10 compétences dans la liste
  if (
    skill &&
    document.getElementById("skillsList").getElementsByClassName("skill-item")
      .length < 10
  ) {
    // Crée un nouvel élément de liste pour ajouter la compétence
    let li = document.createElement("li");
    li.className = "skill-item";
    li.textContent = skill;

    // Crée un bouton pour supprimer la compétence
    let removeBtn = document.createElement("i");
    removeBtn.classList.add("fa-regular", "fa-trash-can");
    removeBtn.onclick = function () {
      // Supprime l'élément parent (la compétence) lorsqu'on clique sur le bouton
      this.parentElement.remove();
      // Vérifie si les conditions pour ajouter/supprimer des compétences sont respectées
      checkInput();
    };

    // Ajoute le bouton de suppression à la compétence
    li.appendChild(removeBtn);
    // Ajoute la compétence à la liste des compétences
    document.getElementById("skillsList").appendChild(li);
    // Vide l'input et redonne le focus pour ajouter d'autres compétences
    input.value = "";
    input.focus();
  }
  // Vérifie les conditions après l'ajout d'une compétence
  checkInput();
});

// Fonction pour capitaliser la première lettre d'une chaîne de caractères
function capitalizeFirstLetter(string) {
  const lowerCaseString = string.toLowerCase(); // Met la chaîne en minuscules
  // Retourne la chaîne avec la première lettre en majuscule et le reste en minuscules
  return lowerCaseString.charAt(0).toUpperCase() + lowerCaseString.slice(1);
}

// Fonction pour gérer le nombre de compétences minimum et maximum autorisées
function checkInput() {
  let list = document.getElementById("skillsList");
  let input = document.getElementById("skillInput");
  // Si le nombre de compétences est inférieur à 5, indique un minimum requis
  if (list.getElementsByClassName("skill-item").length < 5) {
    input.placeholder = "Ajoutez au moins 5 compétences.";
  } else if (list.getElementsByClassName("skill-item").length >= 10) {
    // Si le nombre de compétences atteint 10, désactive l'input
    input.placeholder = "Maximum 10 compétences atteint.";
    input.disabled = true;
  } else {
    // Sinon, permet l'ajout de compétences
    input.placeholder = "Ajoutez une compétence";
    input.disabled = false;
  }
}

// Ajoute un événement "submit" sur le formulaire pour valider les compétences avant envoi
document.querySelector("form").addEventListener("submit", function (event) {
  // Compte le nombre de compétences ajoutées
  let skillCount = document
    .getElementById("skillsList")
    .getElementsByClassName("skill-item").length;
  // Si le nombre de compétences est inférieur à 5, affiche un message d'alerte et empêche l'envoi du formulaire
  if (skillCount < 5) {
    alert("Veuillez ajouter au moins 5 compétences avant de soumettre.");
    event.preventDefault(); // Empêche l'envoi du formulaire
  } else {
    // Crée des champs cachés pour chaque compétence et les ajoute au formulaire
    for (let i = 0; i < skillCount; i++) {
      let hiddenInput = document.createElement("input");
      hiddenInput.type = "hidden";
      hiddenInput.name = "competence" + (i + 1);
      hiddenInput.value =
        document.getElementsByClassName("skill-item")[i].textContent;
      // Ajoute chaque champ caché au formulaire pour l'envoyer avec les données du formulaire
      this.appendChild(hiddenInput);
    }
  }
});
