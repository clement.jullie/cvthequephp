<?php

$entryData = null;
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $csvFilePath = 'hrdata.csv';
    if (($handle = fopen($csvFilePath, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
            if ($data[0] == $id) {
                $entryData = $data;
                break;
            }
        }
        fclose($handle);
    }
    // Formatage de la date de naissance pour le champ input de type date
    $formattedDate = isset($entryData[4]) ? date("Y-m-d", DateTime::createFromFormat('d/m/Y', $entryData[4])->getTimestamp()) : '';

    $competences = [];
    for ($i = 13; $i <= 22; $i++) {
        if (!empty($entryData[$i]) && $entryData[$i] != 'NULL') {
            $competences[] = $entryData[$i];
        }
    }
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aghoratheque - Formulaire</title>
    <!-- ### STYLES ET SCRIPTS EXTERNES -->
    <link rel="stylesheet" href="style.css">
    <script src="https://kit.fontawesome.com/f8d0d04345.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400..700&family=Pacifico&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/vendor/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
    <script src="../../assets/vendor/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
</head>

<body class="background">
    <div class="container mt-5 p-2 bgColorForm">
        <h2 class="text-center m-4">Formulaire d'inscription</h2>
        <form enctype="multipart/form-data" action="./response.php" method="post" class="m-3 d-grid">
            <!-- Champ caché pour transmettre l'ID -->
            <input type="hidden" name="id" value="<?php echo isset($id) ? htmlspecialchars($id) : ''; ?>">
            <div class="row">
                <div class="form-group col m-2">
                    <label for="nom"></label>
                    <input name="nom" type="text" class="form-control" id="nom" placeholder="Nom" value="<?php echo isset($entryData) ? htmlspecialchars($entryData[1]) : ''; ?>" required>
                </div>
                <div class="form-group col m-2">
                    <label for="prenom"></label>
                    <input name="prenom" type="text" class="form-control" id="prenom" placeholder="prenom" value="<?php echo isset($entryData) ? htmlspecialchars($entryData[2]) : ''; ?>" required>
                </div>
            </div>
            <div class="form-group m-2">
                <label class="fw-bolder mb-1" for="dateNaissance">Date de naissance :</label>
                <input name="dateNaissance" type="date" class="form-control" id="dateNaissance" placeholder="jj/mm/aaaa" value="<?php echo htmlspecialchars($formattedDate); ?>" required>
            </div>
            <div class="form-group m-2 mt-0">
                <label for="adresse"></label>
                <input name="adresse" type="text" class="form-control" id="adresse" placeholder="adresse" value="<?php echo isset($entryData[5]) ? htmlspecialchars($entryData[5]) : ''; ?>"></input>
            </div>
            <div class="form-group m-2 mt-0">
                <label for="adresse1"></label>
                <input name="adresse1" type="text" class="form-control" id="adresse1" placeholder="adresse1" value="<?php echo isset($entryData) ? htmlspecialchars($entryData[6]) : ''; ?>"></input>
            </div>
            <div class="row">
                <div class="form-group col m-2">
                    <label for="codePostal"></label>
                    <input name="codePostal" type="text" class="form-control" id="codePostal" placeholder="codePostal" value="<?php echo isset($entryData) ? htmlspecialchars($entryData[7]) : ''; ?>">
                </div>
                <div class="form-group col m-2">
                    <label for="ville"></label>
                    <input name="ville" type="text" class="form-control" id="ville" placeholder="ville" value="<?php echo isset($entryData) ? htmlspecialchars($entryData[8]) : ''; ?>">
                </div>
            </div>
            <div class="form-group m-2">
                <label for="email"></label>
                <input name="email" type="text" class="form-control" id="email" placeholder="email" value="<?php echo isset($entryData) ? htmlspecialchars($entryData[11]) : ''; ?>" required>
            </div>
            <div class="row">
                <div class="form-group col m-2">
                    <label for="telephoneFixe"></label>
                    <input name="telephoneFixe" type="text" class="form-control" id="telephoneFixe" placeholder="telephoneFixe" value="<?php echo isset($entryData) ? htmlspecialchars($entryData[10]) : ''; ?>">
                </div>
                <div class="form-group col m-2">
                    <label for="telephonePortable"></label>
                    <input name="telephonePortable" type="text" class="form-control" id="telephonePortable" placeholder="telephonePortable" value="<?php echo isset($entryData) ? htmlspecialchars($entryData[9]) : ''; ?>" required>
                </div>
            </div>
            <div class="form-group m-2">
                <label for="profil"></label>
                <input name="profil" type="text" class="form-control" id="profil" placeholder="profil" value="<?php echo isset($entryData) ? htmlspecialchars($entryData[12]) : ''; ?>" required>
            </div>
            <div class="container mt-2 p-2">
                <div id="skillsForm">
                    <div class="form-group">
                        <label class="fw-bolder mb-1" for="competences">Compétences</label>
                        <input type="text" id="skillInput" class="form-control" placeholder="Ajoutez une compétence">
                    </div>
                    <button type="button" id="addSkill" class="btn btn-success mt-2">Ajouter</button>
                </div>
                <ul id="skillsList" class="skills-container"></ul>
            </div>
            <div class="form-group m-2">
                <label for="siteWeb"></label>
                <input name="siteWeb" type="text" class="form-control" id="siteWeb" placeholder="siteWeb" value="<?php echo isset($entryData) ? htmlspecialchars($entryData[23]) : ''; ?>">
            </div>
            <div class="form-group m-2">
                <label for="linkedin"></label>
                <input name="linkedin" type="text" class="form-control" id="linkedin" placeholder="linkedin" value="<?php echo isset($entryData) ? htmlspecialchars($entryData[24]) : ''; ?>">
            </div>
            <div class="form-group m-2">
                <label for="viadeo"></label>
                <input name="viadeo" type="text" class="form-control" id="viadeo" placeholder="viadeo" value="<?php echo isset($entryData) ? htmlspecialchars($entryData[25]) : ''; ?>">
            </div>
            <div class="form-group m-2">
                <label for="facebook"></label>
                <input name="facebook" type="text" class="form-control" id="facebook" placeholder="facebook" value="<?php echo isset($entryData) ? htmlspecialchars($entryData[26]) : ''; ?>">
            </div>
            <div class="form-group m-3 m-2 d-flex flex-column justify-content-center">
                <label class="mb-4 fw-bolder fs-5 text" for="cv">CV à transmettre</label>
                <input type="file" name="cv" class="form-control-file fs-6 text" id="cv">
            </div>
            <div class="row justify-content-md-center">
                <button type="submit" class="btn btn-success mt-3 col-2 me-4">Enregistrer</button>
                <!-- <a href="./index.php"></a> -->
                <a href="./index.php" class="btn btn-danger mt-3 col-2 me-4">Annuler</a>
            </div>
        </form>
    </div>
    <script src="script.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var competences = <?php echo json_encode($competences); ?>;
            var skillList = document.getElementById("skillsList");

            competences.forEach(function(skill) {
                addSkillToList(skill);
            });
        });

        function addSkillToList(skill) {
            let li = document.createElement("li");
            li.className = "skill-item";
            li.textContent = skill;

            let removeBtn = document.createElement("i");
            removeBtn.classList.add("fa-regular", "fa-trash-can");
            removeBtn.onclick = function() {
                this.parentElement.remove();
                checkInput();
            };

            li.appendChild(removeBtn);
            document.getElementById("skillsList").appendChild(li);
        }

        function checkInput() {
            let list = document.getElementById("skillsList");
            let input = document.getElementById("skillInput");
            if (list.getElementsByClassName("skill-item").length < 5) {
                input.placeholder = "Ajoutez au moins 5 compétences.";
            } else if (list.getElementsByClassName("skill-item").length >= 10) {
                input.placeholder = "Maximum 10 compétences atteint.";
                input.disabled = true;
            } else {
                input.placeholder = "Ajoutez une compétence";
                input.disabled = false;
            }
        }
    </script>
</body>

</html>