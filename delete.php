<?php
require_once 'function.php';

// Gérer la suppression
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['delete_id'])) {
    $idToDelete = $_POST['delete_id'];
    $csvFilePath = 'hrdata.csv';
    $tempData = array();

    // Lire et collecter les données à conserver
    if (($handle = fopen($csvFilePath, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
            if ($data[0] != $idToDelete) {
                $tempData[] = $data;
            }
        }
        fclose($handle);
    }

    // Réécrire le fichier CSV sans la ligne à supprimer
    if (($handle = fopen($csvFilePath, "w")) !== FALSE) {
        foreach ($tempData as $row) {
            fputcsv($handle, $row, ";");
        }
        fclose($handle);
    }

    $fichier = './' . $idToDelete . '.docx';
    $fichier1 = './' . $idToDelete . '.pdf';

    if (file_exists($fichier)) {

        unlink($fichier);
    } elseif (file_exists($fichier1)) {
        unlink($fichier1);
    } else {
        header("Location: index.php?del=1");
        exit;
    }


    // Rafraîchir la page pour afficher les modifications
    header("Location: index.php?del=1");
    exit;
}
