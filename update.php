<?php
// update.php

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $id = $_POST['id']; // ID de l'entrée à modifier
    // Autres champs du formulaire
    $name = $_POST['nom'];
    $email = $_POST['email'];
    $telephoneFixe = $_POST['telephoneFixe'];
    $telephonePortable = $_POST['telephonePortable'];
    $profil = $_POST['profil'];
    $siteWeb = $_POST['siteWeb'];
    $linkedin = $_POST['linkedin'];
    $viadeo = $_POST['viadeo'];
    $facebook = $_POST['facebook'];
    $prenom = $_POST['prenom'];
    $dateNaissance = $_POST['dateNaissance'];
    $adresse = $_POST['adresse'];
    $adresse1 = $_POST['adresse1'];
    $codePostal = $_POST['codePostal'];
    $ville = $_POST['ville'];
    $competences = [];
    $competences[] = $_POST['competence1'];
    $competences[] = $_POST['competence2'];
    $competences[] = $_POST['competence3'];
    $competences[] = $_POST['competence4'];
    $competences[] = $_POST['competence5'];
    $competences[] = $_POST['competence6'];
    $competences[] = $_POST['competence7'];
    $competences[] = $_POST['competence8'];
    $competences[] = $_POST['competence9'];
    $competences[] = $_POST['competence10'];


    // Lire les données actuelles du fichier CSV
    $file = fopen('hrdata.csv', 'r');
    $data = [];
    while (($row = fgetcsv($file)) !== FALSE) {
        $data[] = $row;
    }
    fclose($file);

    // Trouver et modifier l'entrée
    foreach ($data as &$row) {
        if ($row[0] == $id) {
            $row[1] = $name;
            $row[2] = $prenom;
            $row[4] = $dateNaissance;
            $row[5] = $adresse;
            $row[6] = $adresse1;
            $row[7] = $codePostal;
            $row[8] = $ville;
            $row[11] = $email;
            $row[10] = $telephoneFixe;
            $row[9] = $telephonePortable;
            $row[12] = $profil;
            $row[23] = $siteWeb;
            $row[24] = $linkedin;
            $row[25] = $viadeo;
            $row[26] = $facebook;
            $row[13] = $competences[0];
            $row[14] = $competences[1];
            $row[15] = $competences[2];
            $row[16] = $competences[3];
            $row[17] = $competences[4];
            $row[18] = $competences[5];
            $row[19] = $competences[6];
            $row[20] = $competences[7];
            $row[21] = $competences[8];
            $row[22] = $competences[9];
        }
    }

    // Écrire les données mises à jour dans le fichier CSV
    $file = fopen('hrdata.csv', 'w');
    foreach ($data as $row) {
        fputcsv($file, $row);
    }
    fclose($file);

    // Redirection ou message de succès
    header('Location: index.php?status=success');
    exit;
}
