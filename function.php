<?php

function calculerAge($dateNaissance) {
  try {
      // Convertit d'abord la date du format CSV 'd/m/Y' au format 'Y-m-d'
      $dateDeNaissance = DateTime::createFromFormat('d/m/Y', $dateNaissance);
      if ($dateDeNaissance === false) {
          throw new Exception("Format de date invalide.");
      }

      $dateActuelle = new DateTime();
      $interval = $dateDeNaissance->diff($dateActuelle);
      return $interval->y;
  } catch (Exception $e) {
      // Gestion d'erreur si la création de DateTime échoue
      error_log('Erreur lors de la création de DateTime: ' . $e->getMessage());
      return null; // ou gérez l'erreur comme il convient à votre application
  }
}

// Fonction pour formater les dates de naissance dans un autre format
function formatDate($date, $format = 'd/m/Y') {
  $dateTime = DateTime::createFromFormat('Y-m-d', $date);
  if ($dateTime === false) {
    return 'Date invalide';
  }
  return $dateTime->format($format);
}

// Fonction pour reformater les dates avant l'enregistrement
function reformatDateBeforeSaving($date) {
  $dateTime = DateTime::createFromFormat('Y-m-d', $date);
  if ($dateTime === false) {
    return false;
  }
  return $dateTime->format('d/m/Y');
}

?>