<?php
// Inclusion de fichiers pour les fonctions et la suppression
require_once 'function.php';
require_once 'delete.php';

if (isset($_GET["add"])) {
?><script>
    alert("L'utilisateur à bien été ajouté à votre CVthéque");
  </script>
<?php }
if (isset($_GET["del"])) {
?><script>
    alert("L'utilisateur à bien été supprimé");
  </script>
<?php }

// Variables initiales
$tab = array(); // Tableau principal pour stocker toutes les données du fichier CSV
$filteredResults = array(); // Tableau pour stocker les résultats filtrés par recherche
$searchActive = false; // Booléen pour déterminer si une recherche est active

// Vérifier si le formulaire de recherche a été soumis en méthode POST
if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST['search'])) {
  $searchTerm = strtolower(trim($_POST['search'])); // Récupérer le terme de recherche en minuscule et nettoyé
  $searchActive = true; // Activer le mode recherche
}

// Ouvrir le fichier CSV
if (($handle = fopen("hrdata.csv", "r")) !== FALSE) {
  // Parcourir chaque ligne du fichier
  while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
    // Si une recherche est active, filtrer les données
    if ($searchActive) {
      foreach ($data as $field) {
        if (strpos(strtolower($field), $searchTerm) !== FALSE) {
          $filteredResults[] = $data; // Ajouter la ligne correspondante aux résultats filtrés
          break;
        }
      }
    } else {
      // Sinon, ajouter toutes les données dans le tableau principal
      $tab[] = $data;
    }
  }
  fclose($handle); // Fermer le fichier après traitement
}

// Déterminer le tableau des données à afficher, en fonction de l'état de la recherche
$tabindex = $searchActive ? $filteredResults : array_slice($tab, 1); // Exclure la première ligne (en-têtes) si pas de recherche
?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>AGHORATHEQUE</title>
  <!-- Inclusion de styles et de scripts externes -->
  <link rel="stylesheet" href="style.css">
  <script src="https://kit.fontawesome.com/f8d0d04345.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400..700&family=Pacifico&display=swap" rel="stylesheet">
  <script src="script.js" defer></script>
</head>

<body class="background">
  <div class="titre d-flex justify-content-evenly flex-wrap">
    <div class="ms-4">
      <h1 class="mb-4 mt-4 ms-4 p-0">Aghorathéque</h1>
      <h3 class="text-center mb-4 p-0">Ensemble pour mieux Agir</h3>
    </div>
    <nav class="navbar mb-4 mt-4 p-0">
      <div>
        <!-- Barre de recherche -->
        <form class="d-flex" role="search" action="<?php print $_SERVER['PHP_SELF']; ?>" method="post">
          <input class="form-control me-2 search" type="search" name="search" placeholder="Rechercher" aria-label="Search">
          <button class="btn btn-outline-success btnSearch" type="submit">Rechercher</button>
        </form>
      </div>
    </nav>
  </div>
  <div class="container d-flex justify-content-evenly">
    <!-- Menu déroulant pour le tri -->
    <div class="input-group mb-3 w-50 p-2">
      <label class="input-group-text" for="inputGroupSelect01">Trier</label>
      <select class="form-select" id="inputGroupSelect01">
        <option value="0" selected>Choisissez...</option>
        <option value="1">Trier par nom de A à Z</option>
        <option value="2">Trier par nom de Z à A</option>
        <option value="3">Trier par ville de A à Z</option>
        <option value="4">Trier par ville de Z à A</option>
        <option value="7">Trier par âge en ordre croissant de A à Z</option>
        <option value="8">Trier par âge en ordre décroissant de Z à A</option>
      </select>
    </div>
    <!-- Bouton pour ajouter un utilisateur -->
    <a href="./form.php" class="tailleBouton">
      <button type="button" class="btn btn-light buttonAppliquer rounded-5 m-0 p-2"><i class="fa-solid fa-user-plus fa-2x logoAdd"></i></button>
    </a>
  </div>
  <main class="d-flex flex-wrap justify-content-center">
    <!-- Boucle pour chaque utilisateur à afficher -->
    <?php foreach ($tabindex as $index => $value) { ?>
      <div class="flip-card m-4">
        <div class="flip-card-inner d-flex flex-row justify-content-evenly">
          <!-- Recto de la carte -->
          <div class="flip-card-front rounded-4">
            <i class="fa-solid fa-user fa-6x mt-4"></i>
            <h3 class="mt-2"><?php print_r($value[2] . " " . $value[1]); ?></h3>
            <p class="fs-5 colorTexte"> <?php print_r($value[12]); ?></p>
            <p class="texteAge"><?php
                                $dateNaissance = $value[4];
                                $age = calculerAge($dateNaissance); // Calculer l'âge avec une fonction externe
                                if ($age != 'NULL') {
                                  print($age . " ans");
                                }
                                ?></p>
            <p class="text-bold"><?php if ($value[8] != 'NULL' && $value[$i] != "") {
                                    print_r(strtoupper($value[8]));
                                  } ?></p>
            <?php
            // Affichage des compétences sous forme de badges
            for ($i = 13; $i <= 22; $i++) {
              if ($value[$i] != 'NULL' && $value[$i] != "") {
            ?> <span type="button" class="badge text-bg-secondary m-1"><i class="fa-solid fa-tag"></i> <?php print_r($value[$i] . " "); ?></span>
            <?php }
            }
            ?>
          </div>
          <!-- Verso de la carte -->
          <div class="flip-card-back rounded-4">
            <h3 class="mt-4"><?php print_r($value[2] . " " . $value[1]); ?></h3>
            <p class="m-4"><?php if ($value[5] != 'NULL' && $value[$i] != "") {
                              print_r($value[5]);
                            } ?></p>
            <p class="m-4"><?php if ($value[7] != 'NULL' && $value[$i] != "") {
                              print_r($value[7] . " ");
                            }
                            if ($value[8] != 'NULL' && $value[$i] != "") {
                              print_r($value[8]);
                            } ?></p>
            <p class="fs-5 text"><?php if ($value[9] != 'NULL' && $value[$i] != "") {
                                    print_r($value[9]);
                                  } ?></p>
            <p class="fs-5 text"><?php if ($value[10] != 'NULL' && $value[$i] != "") {
                                    print_r($value[10]);
                                  } ?></p>
            <p class="fs-5 text"><?php if ($value[11] != 'NULL' && $value[$i] != "") {
                                    print_r($value[11]);
                                  } ?></p>
            <p class="text-center mt-5 d-flex justify-content-center">
              <a href="mailto:<?php print $value[11]; ?>?subject=Contacter nous&body=Votre Demande" class="link-noir">
                <i class="fa-regular fa-envelope logo fs-4"></i>
              </a>
              <?php
              // Vérifier et afficher les liens vers les fichiers PDF ou DOCX si disponibles
              $pdfname = './' . $value[0] . ".pdf";
              $docxname = './' . $value[0] . ".docx";

              if (file_exists($pdfname)) {
              ?> <a href="<?php print $pdfname ?>" target="_blank"><i class="fa-regular fa-file-pdf logo fs-4 text-black"></i></a> <?php } elseif (file_exists($docxname)) {
                                                                                                                                    ?><a href="<?php print $docxname ?>" target="_blank"><i class="fa-regular fa-file-word logo fs-4 text-black"></i></a>

              <?php
                                                                                                                                  }
              ?>
            </p>
            <div class="mt-5 d-flex justify-content-center">
              <!-- Lien vers le formulaire de modification -->
              <form action="POST">
                <a href="form.php?id=<?php echo htmlspecialchars($value[0]); ?>" class="btn btn-success m-3 btnFlipCard">Modifier</a>
              </form>
              <!-- Formulaire pour supprimer l'utilisateur -->
              <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <input type="hidden" name="delete_id" value="<?php echo htmlspecialchars($value[0]); ?>">
                <button type="submit" class="btn btn-danger m-3 btnFlipCard">Supprimer</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  </main>
  <script>
    document.addEventListener("DOMContentLoaded", function() {
      const selectElement = document.getElementById("inputGroupSelect01");

      selectElement.addEventListener("change", function(event) {
        const selectedValue = event.target.value;

        if (selectedValue === "0") {
          window.location.href = window.location.href.split('?')[0];
          return;
        }

        const cards = document.querySelectorAll(".flip-card");
        const cardsArray = Array.from(cards);

        let compareFunction;

        switch (selectedValue) {
          case "1":
            compareFunction = (a, b) => {
              const nameA = a.querySelector(".flip-card-front h3").innerText.toUpperCase();
              const nameB = b.querySelector(".flip-card-front h3").innerText.toUpperCase();
              return nameA.localeCompare(nameB);
            };
            break;
          case "2":
            compareFunction = (a, b) => {
              const nameA = a.querySelector(".flip-card-front h3").innerText.toUpperCase();
              const nameB = b.querySelector(".flip-card-front h3").innerText.toUpperCase();
              return nameB.localeCompare(nameA);
            };
            break;
          case "3":
            compareFunction = (a, b) => {
              const cityA = a.querySelector(".flip-card-front p.text-bold").innerText.toUpperCase();
              const cityB = b.querySelector(".flip-card-front p.text-bold").innerText.toUpperCase();
              return cityA.localeCompare(cityB);
            };
            break;
          case "4":
            compareFunction = (a, b) => {
              const cityA = a.querySelector(".flip-card-front p.text-bold").innerText.toUpperCase();
              const cityB = b.querySelector(".flip-card-front p.text-bold").innerText.toUpperCase();
              return cityB.localeCompare(cityA);
            };
            break;
          case "7":
            compareFunction = (a, b) => {
              const ageA = parseInt(a.querySelector(".flip-card-front p.texteAge").innerText);
              const ageB = parseInt(b.querySelector(".flip-card-front p.texteAge").innerText);
              return ageA - ageB;
            };
            break;
          case "8":
            compareFunction = (a, b) => {
              const ageA = parseInt(a.querySelector(".flip-card-front p.texteAge").innerText);
              const ageB = parseInt(b.querySelector(".flip-card-front p.texteAge").innerText);
              return ageB - ageA;
            };
            break;
          default:
            return;
        }

        const sortedCards = cardsArray.sort(compareFunction);
        const container = document.querySelector("main.d-flex.flex-wrap.justify-content-center");

        container.innerHTML = "";
        sortedCards.forEach(card => {
          container.appendChild(card);
        });
      });
    });
  </script>
</body>

</html>