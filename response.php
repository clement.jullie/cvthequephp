<?php

// Inclut les fonctions nécessaires
require_once 'function.php';
// Fonction pour nettoyer et valider les données reçues
function sanitizeData($data)
{
    $data = trim($data); // Supprime les espaces au début et à la fin
    $data = stripslashes($data); // Retire les antislashs
    $data = htmlspecialchars($data, ENT_SUBSTITUTE | ENT_HTML5); // Convertit les caractères spéciaux en entités HTML
    return empty($data) ? 'NULL' : $data; // Retourne 'NULL' si la donnée est vide
}

if (isset($_POST['id']) && ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST['id'] != "")) {
    $id = $_POST['id']; // ID de l'entrée à modifier
    // Autres champs du formulaire
    $name = $_POST['nom'];
    $email = $_POST['email'];
    $telephoneFixe = $_POST['telephoneFixe'];
    $telephonePortable = $_POST['telephonePortable'];
    $profil = $_POST['profil'];
    $siteWeb = $_POST['siteWeb'];
    $linkedin = $_POST['linkedin'];
    $viadeo = $_POST['viadeo'];
    $facebook = $_POST['facebook'];
    $prenom = $_POST['prenom'];
    $dateNaissance = $_POST['dateNaissance'];
    $adresse = $_POST['adresse'];
    $adresse1 = $_POST['adresse1'];
    $codePostal = $_POST['codePostal'];
    $ville = $_POST['ville'];
    $competences = [];
    $competences[] = $_POST['competence1'];
    $competences[] = $_POST['competence2'];
    $competences[] = $_POST['competence3'];
    $competences[] = $_POST['competence4'];
    $competences[] = $_POST['competence5'];
    $competences[] = $_POST['competence6'];
    $competences[] = $_POST['competence7'];
    $competences[] = $_POST['competence8'];
    $competences[] = $_POST['competence9'];
    $competences[] = $_POST['competence10'];


    // Lire les données actuelles du fichier CSV
    $file = fopen('hrdata.csv', 'r');
    $data = [];
    while (($row = fgetcsv($file, 1000, ";")) !== FALSE) {
        $data[] = $row;
    }
    fclose($file);

    // Trouver et modifier l'entrée
    foreach ($data as $index => $row) {
        if ($row[0] == $id) {
            $data[$index][1] = sanitizeData($name);
            $data[$index][2] = sanitizeData($prenom);
            $data[$index][4] = reformatDateBeforeSaving($dateNaissance);
            $data[$index][5] = sanitizeData($adresse);
            $data[$index][6] = sanitizeData($adresse1);
            $data[$index][7] = sanitizeData($codePostal);
            $data[$index][8] = sanitizeData($ville);
            $data[$index][11] = sanitizeData($email);
            $data[$index][10] = sanitizeData($telephoneFixe);
            $data[$index][9] = sanitizeData($telephonePortable);
            $data[$index][12] = sanitizeData($profil);
            $data[$index][23] = sanitizeData($siteWeb);
            $data[$index][24] = sanitizeData($linkedin);
            $data[$index][25] = sanitizeData($viadeo);
            $data[$index][26] = sanitizeData($facebook);
            $data[$index][13] = sanitizeData($competences[0]);
            $data[$index][14] = sanitizeData($competences[1]);
            $data[$index][15] = sanitizeData($competences[2]);
            $data[$index][16] = sanitizeData($competences[3]);
            $data[$index][17] = sanitizeData($competences[4]);
            $data[$index][18] = sanitizeData($competences[5]);
            $data[$index][19] = sanitizeData($competences[6]);
            $data[$index][20] = sanitizeData($competences[7]);
            $data[$index][21] = sanitizeData($competences[8]);
            $data[$index][22] = sanitizeData($competences[9]);
        }
    }

    // Écrire les données mises à jour dans le fichier CSV
    $file = fopen('hrdata.csv', 'w');
    foreach ($data as $row) {
        fwrite($file, implode(";", $row) . PHP_EOL);
    }
    fclose($file);

    // Redirection ou message de succès
    header('Location: index.php?status=success');
    exit;
} else {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {




        // Récupère l'ID à modifier s'il est fourni, sinon définit comme null
        $idToEdit = isset($_POST['id']) && !empty($_POST['id']) ? sanitizeData($_POST['id']) : null;

        // Chemin du fichier CSV
        $csvFilePath = 'hrdata.csv';

        // Trouve le dernier ID utilisé dans le fichier CSV pour définir le prochain ID
        $lastId = 0;
        if (($fileHandle = fopen($csvFilePath, 'r')) !== FALSE) {
            // Parcourt le fichier ligne par ligne pour trouver le dernier ID
            while (($data = fgetcsv($fileHandle, 1000, ";")) !== FALSE) {
                $lastId = $data[0]; // L'ID est la première colonne
            }
            fclose($fileHandle); // Ferme le fichier
        }

        $newId = $lastId + 1; // Incrémente l'ID pour la nouvelle entrée

        // Récupère et nettoie les données du formulaire
        $nom = sanitizeData($_POST['nom'] ?? '');
        $prenom = sanitizeData($_POST['prenom'] ?? '');
        $dateNaissance = formatDate($_POST['dateNaissance'] ?? '');
        $age = calculerAge($dateNaissance);
        $adresse = sanitizeData($_POST['adresse'] ?? '');
        $adresse1 = sanitizeData($_POST['adresse1'] ?? '');
        $codePostal = sanitizeData($_POST['codePostal'] ?? '');
        $ville = sanitizeData($_POST['ville'] ?? '');
        $telephonePortable = sanitizeData($_POST['telephonePortable'] ?? '');
        $telephoneFixe = sanitizeData($_POST['telephoneFixe'] ?? '');
        $email = sanitizeData($_POST['email'] ?? '');
        $profil = sanitizeData($_POST['profil'] ?? '');
        $siteWeb = sanitizeData($_POST['siteWeb'] ?? '');
        $linkedin = sanitizeData($_POST['linkedin'] ?? '');
        $viadeo = sanitizeData($_POST['viadeo'] ?? '');
        $facebook = sanitizeData($_POST['facebook'] ?? '');

        // Récupère les compétences fournies par l'utilisateur
        $competences = [];
        for ($i = 1; $i <= 10; $i++) {
            // Ajoute chaque compétence nettoyée ou 'NULL' si non fournie
            $competences[] = sanitizeData($_POST["competence$i"] ?? 'NULL');
        }

        // Concatène toutes les informations dans un tableau pour les ajouter au fichier CSV
        $fields = array_merge([$newId, $nom, $prenom, $age, $dateNaissance, $adresse, $adresse1, $codePostal, $ville, $telephonePortable, $telephoneFixe, $email, $profil, $siteWeb, $linkedin, $viadeo, $facebook], $competences);
        $csvLine = implode(";", $fields) . "\n"; // Crée une ligne CSV

        // Ouvre le fichier CSV en mode "append" (ajout) et écrit la nouvelle ligne
        if ($fileHandle = fopen($csvFilePath, 'a')) {
            fwrite($fileHandle, $csvLine); // Écrit dans le fichier
            fclose($fileHandle); // Ferme le fichier

            // Redirige l'utilisateur vers la page principale et affiche un message
            header("Location: index.php?add=1");
            echo '<script>alert("Données enregistrées avec succès");</script>';
        } else {
            // Affiche une erreur si l'écriture dans le fichier échoue
            echo "Erreur lors de l'écriture dans le fichier.";
        }
    } else {
        // Redirige vers le formulaire si la requête n'est pas POST
        header("Location: index.php");
        exit;
    }

    // Gestion du téléchargement de fichiers (par exemple, un CV)
    $target_dir = "./"; // Répertoire cible pour l'enregistrement
    $target_file = $target_dir . basename($_FILES["cv"]["name"]); // Chemin complet du fichier
    $uploadOk = 1; // Flag pour vérifier si le téléchargement peut avoir lieu
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION)); // Type de fichier

    // Change le nom du fichier pour correspondre au nouvel ID
    $changeName = $target_dir . $newId . "." . $imageFileType;

    // Vérifie si le fichier existe déjà
    if (file_exists($target_file)) {
        echo "Désolé, le fichier existe déjà.";
        $uploadOk = 0;
    }

    // Vérifie la taille du fichier (limite à 500 KB)
    if ($_FILES["cv"]["size"] > 500000) {
        echo "Désolé, le fichier est trop volumineux.";
        $uploadOk = 0;
    }

    // Vérifie les formats autorisés (PDF et DOCX uniquement)
    if ($imageFileType != "pdf" && $imageFileType != "docx") {
        echo "Désolé, seuls les fichiers PDF et DOCX sont autorisés.";
        $uploadOk = 0;
    }

    // Vérifie que tout est correct avant le téléchargement
    if ($uploadOk == 0) {
        echo "Désolé, votre fichier n'a pas été téléchargé.";
    } else {
        // Déplace le fichier téléchargé vers le répertoire cible et change son nom
        if (move_uploaded_file($_FILES["cv"]["tmp_name"], $changeName)) {
            echo "Le fichier " . htmlspecialchars(basename($_FILES["cv"]["name"])) . " a été téléchargé.";
        } else {
            // Affiche une erreur en cas d'échec du téléchargement
            echo "Désolé, une erreur s'est produite lors du téléchargement de votre fichier.";
        }
    }
}
